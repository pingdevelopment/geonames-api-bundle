GEONAMES SYMFONY2 BUNDLE
===========

This bundle works with Symfony 2.x to allow configuration of the API as a service.

Requirements
============

- Apache 2 Web server
- PHP 5.x configured with the following extensions:
  - CUrl


Configuration
=============

The following elements are valid in your YAML configuration files:


```
#!yaml

geonames_api:
    username: demo
    maxResults: 50
```


Only username is required and it should be set to the username you registered with
on geonames.org. The 'maxResults' value defaults to 10 (same as the geonames.org
default) and is optional.

How to Use the Bundle
=====================

The API wrapper is provided as a service container. To obtain an instance of the
API wrapper, you need to get the container reference (from a Controller action)
like so:


```
#!php

$api = $this->get('geonames_api.zipcode_lookup');
$api->addFilter(PostalCodeSearch::FILTER_PLACE_NAME_STARTS_WITH, $city);
$api->addFilter(PostalCodeSearch::FILTER_STATE_CODE, $state);
$api->lookup();
$response = $api->getArrayResponse();
print_r($response);
```

The following lookups are available:

* geonames_api.zipcode_lookup
* geonames_api.radius_lookup


Authors
=======
Peter Adams <http://www.pingdevelopment.com>

License
=======

The MIT License

Copyright (c) 2014

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.